package com.sqlstream.udx;

import com.sqlstream.jdbc.StreamingPreparedStatement;
import com.sqlstream.jdbc.StreamingResultSet;
import com.sqlstream.jdbc.StreamingResultSet.RowEvent;
import com.sqlstream.plugin.impl.AbstractBaseUdx;

import java.sql.*;
import java.util.LinkedHashMap;
import java.util.logging.Logger;
import java.util.logging.Level;

public class PivotUdx
    extends
    AbstractBaseUdx
{
    public static final Logger tracer =
        Logger.getLogger(PivotUdx.class.getName());

    private String[] uniqueColumnNames;
    private String keyColumnName;
    private String valueColumnName;
    private int uniqueCount;

    String[] lastRowUID = null;
    String[] currentRowUID = null;

    StreamingPreparedStatement out;

    private long rtime;
    private long last_rtime = 0;
    boolean hasRow = false;

    private PivotUdx(
        Logger tracer
            , ResultSet inputRows
            , String uniqueColumnNames
            , String keyColumnName
            , String valueColumnName
            , PreparedStatement results)
        throws SQLException
    {
        super(tracer, inputRows, results);

        this.uniqueColumnNames = uniqueColumnNames.split(",");
        uniqueCount = this.uniqueColumnNames.length;
        this.keyColumnName = keyColumnName;
        this.valueColumnName = valueColumnName;

        tracer.fine("Inializing Pivot UDX: uidNames="+arrayToString(this.uniqueColumnNames)+", len="+uniqueCount+", key="+keyColumnName+", value="+valueColumnName);
    }

    private String arrayToString(String[] uid) {
        String sep = "[ ";
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < uid.length; i++) {
            sb = sb.append(sep+uid[i]);
            sep = ", ";
        }
        sb = sb.append("]");
        return sb.toString();
    }

    /**
     * Turn a narrow stream into a longer/narrower stream by folding selected columns
     * - selected columns are removed
     * - two input columns are removed - one for the column name, one for the column value
     * - all columns selected for pivot should be the same type (not enforced)
     * - for N columns being pivoted, 1 row will be emitted for every N rows received
     * - we emit a new row when we start receiving components of the next row, or when we receive a new rowtime bound
     * -
     * @param inputRows
     * @param uniqueColumnNames
     * @param
     * @param results
     * @throws SQLException
     */
    public static void execute
            ( ResultSet inputRows
                    , String uniqueColumnNames
                    , String keyColumnName
                    , String valueColumnName
                    , PreparedStatement results
            ) throws SQLException
    {
        PivotUdx udx = new PivotUdx(tracer, inputRows, uniqueColumnNames, keyColumnName, valueColumnName, results);
        udx.executeSimplepivot();
    }

    /**
     * emit the row that has been built up
     */

    private void emitRow() throws SQLException {
        tracer.finer("emit row");
        out.executeUpdate();
        hasRow = false;
    }

    /**
     * get the outputr column position from the colMap already set up by AbstractBaseUDX
     * @param columnName
     * @return
     */
    private int getOutputColumnIndex(String columnName) {
        int[] colMetadata = colMap.get(columnName);

        if (colMetadata == null) {
            // column isn't included in the output
            return 0;
        } else {
            return colMetadata[0];
        }

    }


    private void executeSimplepivot() throws SQLException {
        StreamingResultSet in = (StreamingResultSet)inputRows;
        out = (StreamingPreparedStatement)results;

        ResultSetMetaData rsmd = results.getMetaData();
        
        createPassMap();

        lastRowUID = new String[uniqueCount];
        currentRowUID = new String[uniqueCount];
        String[] swapUID = new String[uniqueCount];

        // set index to the key and value output columns
        int[] uniqueIndex = new int[uniqueCount];  // which input column for each pivot column


        // index into UID column positions
        for (int ucol = 0; ucol < uniqueCount; ucol++) {
            uniqueIndex[ucol] = inputRows.findColumn(uniqueColumnNames[ucol]);

            // set meaningless values for UIDs that hopefully will never match real data - avoid playing with nulls
            lastRowUID[ucol] = "*??*";
            currentRowUID[ucol] = "?**?";
        }

        // index into kev, value and rowtime
        int keyColIndex = inputRows.findColumn(keyColumnName);
        int valueColIndex = inputRows.findColumn(valueColumnName);
        int rowtimeIndex = inputRows.findColumn("ROWTIME");

        // map key names to output column positions
        LinkedHashMap<String,Integer> valueColumnMap = new LinkedHashMap<String,Integer>();

        // TODO allow for timeout (if there is a timeout, emit row if any)

        while (!out.isClosed()) {
            RowEvent e = in.nextRowOrRowtime(1000);

            switch (e) {
            case EndOfStream:
                if (hasRow) emitRow();
                out.close();
                return;  // end of input

            case Timeout:
                continue;  // timeout ??

            case NewRow:
                rtime = in.getTimestampInMillis(rowtimeIndex);

                // get the UID for this row
                for (int i = 0; i < uniqueCount; i++) {
                    currentRowUID[i] = in.getString(uniqueIndex[i]);
                }

                if (tracer.isLoggable(Level.FINER)) {
                    tracer.finer("new row at "+rtime + " uid = "+arrayToString(currentRowUID)+", hasRow="+String.valueOf(hasRow)+", last_rtime="+last_rtime+", lastRowUID="+arrayToString(lastRowUID));
                }

                // Have we got a row already being built up?
                if (hasRow) {
                    // there is a row in progress

                    if (rtime != last_rtime) {
                        // we must be on a new row
                        emitRow();
                    } else {
                        // we need to check whether the UID has changed since the last row
                        boolean match = true;

                        for (int i = 0; i < uniqueCount; i++) {
                            //TODO: this relies on mandatory UID columns - so compare nulls?
                            boolean colmatch = (currentRowUID[i].equals(lastRowUID[i]));
                            match &= colmatch;
                        }

                        if (!match) {
                            tracer.fine("new UID found:"+arrayToString(currentRowUID));
                            // we must have a new row
                            emitRow();
                        }

                    }

                }

                if (!hasRow) {
                    // this is the first incoming row in the group
                    // TODO: is this included in transferColumns
                    //out.setTimestampInMillis(1,rtime);

                    // copy all the passthrough columns
                    // TODO: given we are transferring potentially many columns multiple times, is there setup we could do just once?
                    tracer.fine("first row in group");
                    transferColumns(inputRows, null, results, passMap);
                    hasRow = true;
                }

                // now set the appropriate output column for the given key/value
                String valueColumnName = in.getString(keyColIndex);

                int outputValueColumnIndex = getOutputColumnIndex(valueColumnName);
                if (tracer.isLoggable(Level.FINER)) {
                    tracer.finer("Key="+valueColumnName+",key col#="+keyColIndex+", output col#="+outputValueColumnIndex);
                }

                if (outputValueColumnIndex > 0) {
                    // we found the column in the output (if not, we will ignore it)
                    Object value = in.getObject(valueColIndex);
                    out.setObject(outputValueColumnIndex, value);
                    if (tracer.isLoggable(Level.FINER)) {
                        tracer.finer("Set col# " + outputValueColumnIndex + " to " + value.toString());
                    }
                }

                // swap UID arrays; currentRowUID content will be overridden in next row.
                // this avoids creating a new String[] for every row
                
                swapUID = lastRowUID;
                lastRowUID = currentRowUID;
                currentRowUID = swapUID;

                last_rtime = rtime;

                break;

            case NewRowtimeBound:
                if (hasRow) emitRow();
                Timestamp rtbound = in.getRowtimeBound();
                rtime = rtbound.getTime();
                tracer.finest("rowtime bound at:"+rtime);
                out.setRowtimeBound(rtbound);
                continue;
            }
        }
    }
}
