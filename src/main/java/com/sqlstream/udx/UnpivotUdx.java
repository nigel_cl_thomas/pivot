package com.sqlstream.udx;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Logger;

import com.sqlstream.jdbc.StreamingPreparedStatement;
import com.sqlstream.jdbc.StreamingResultSet;
import com.sqlstream.jdbc.StreamingResultSet.RowEvent;
import com.sqlstream.plugin.impl.AbstractBaseUdx;

public class UnpivotUdx
    extends
    AbstractBaseUdx
{
    public static final Logger tracer =
        Logger.getLogger(UnpivotUdx.class.getName());

    private String[] unpivotColumnNames;
    private String keyColumnName;
    private String valueColumnName;
    private String indexColumnName;
    private int unpivotCount;

    /**
     * Support UnpivotByName - listing out the columns to be unpivoted
     * @param tracer
     * @param inputRows
     * @param unpivotColumnNames
     * @param keyColumnName
     * @param valueColumnName
     * @param results
     * @throws SQLException
     */
    private UnpivotUdx(
        Logger tracer
            , ResultSet inputRows
            , String[] unpivotColumns
            , String keyColumnName
            , String indexColumnName
            , String valueColumnName
            , PreparedStatement results)
        throws SQLException
    {
        super(tracer, inputRows, results);

        this.unpivotColumnNames = unpivotColumns;
        unpivotCount = unpivotColumns.length;
        this.keyColumnName = keyColumnName;
        this.valueColumnName = valueColumnName;
        this.indexColumnName = indexColumnName;
    }

    /**
     * Turn a wide stream into a longer/narrower stream by unfolding selected columns
     * - selected columns are removed
     * - two columns are added - one for the column name, one for the column value
     * - all columns selected for unpivot should be the same type (not enforced)
     * - for N columns being unpivoted, N rows will be emitted (including columns that have null values)
     * -
     * @param inputRows
     * @param unpivotColumnNames  - comma sepatated list of columns to be unpivoted
     * @param keyColumnName       - column that we unpivot on
     * @param valueColumnName     - column we write unpivoted columns to
     * @param indexColumnName     - column we write the ordinal position (1-N) to
     * @param results
     * @throws SQLException
     */
    public static void unpivotByName
    ( ResultSet inputRows
            , String unpivotColumnNames
            , String keyColumnName
            , String indexColumnName
            , String valueColumnName
            , PreparedStatement results
    ) throws SQLException
    {
        String unpivotColumns[] = unpivotColumnNames.split(",");

        // we check that we don't repeat any column names
        for (int i = 1; i < unpivotColumnNames.length(); i++) {
            for (int j = 0; j < i; j++) {
                if (unpivotColumns[i].equals(unpivotColumns[j])) {
                    throw new SQLException("Columns "+j+" and "+i+" are named the same: "+unpivotColumns[i]);
                }
            }
        }
        UnpivotUdx udx = new UnpivotUdx(tracer, inputRows, unpivotColumns, keyColumnName, indexColumnName, valueColumnName, results);
        udx.executeUnpivot();
    }

    /**
     * Turn a wide stream into a longer/narrower stream by unfolding selected columns
     * - selected columns are removed
     * - two columns are added - one for the column name, one for the column value
     * - all columns selected for unpivot should be the same type (not enforced)
     * - for N columns being unpivoted, N rows will be emitted (including columns that have null values)
     * -
     * @param inputRows
     * @param unpivotCount        - count of trailing columns to be unpivoted
     * @param keyColumnName       - column wew write the key (name) to
     * @param valueColumnName     - column we write unpivoted column values to
     * @param indexColumnName     - column we write the ordinal position (1-N) to
     * @param results
     * @throws SQLException
     */
    public static void unpivotByCount
            ( ResultSet inputRows
            , int unpivotCount
            , String keyColumnName
            , String indexColumnName
            , String valueColumnName
            , PreparedStatement results
            ) throws SQLException
    {
        // derive the unpivot column names as the last <N> in the result set metadata
        String[] unpivotColumnNames = new String[unpivotCount];
        ResultSetMetaData rsmd = inputRows.getMetaData();
        int columnCount = rsmd.getColumnCount();

        if (columnCount < unpivotCount+1 ) {
            throw new SQLException(("unpivotCount="+unpivotCount+" but there are only "+columnCount+" input columns"));
        }
        // rsmd.getColumnName starts at 1, not zero

        for (int c = columnCount - unpivotCount + 1, uc = 0; uc < unpivotCount; c++, uc++) {
            unpivotColumnNames[uc] = rsmd.getColumnName(c);
        }

        // FOR TEST
        // if (1 == 1) throw new SQLException("Columns:"+ Arrays.toString(unpivotColumnNames));

        UnpivotUdx udx = new UnpivotUdx(tracer, inputRows, unpivotColumnNames, keyColumnName, indexColumnName, valueColumnName, results);
        udx.executeUnpivot();
    }

    private int getOutputColumnIndex(String columnName) {
        int[] colMetadata = colMap.get(columnName);

        if (colMetadata == null) {
            // column isn't included in the output
            return 0;
        } else {
            return colMetadata[0];
        }

    }

    /***
     * Actually do the unfold
     *
     * For unpivotByCount we are wasting a bit of time by searching for the columns even though we already new
     * the ordering
     *
     *
     * @throws SQLException
     */
    public void executeUnpivot() throws SQLException {
        StreamingResultSet in = (StreamingResultSet)inputRows;
        StreamingPreparedStatement out = (StreamingPreparedStatement)results;
        ResultSetMetaData rsmd = in.getMetaData();

        // set index to the key and value output columns
        int[] unpivotIndex = new int[unpivotCount];  // which input column for each unpivot column

        createPassMap();

        int keyColIndex = getOutputColumnIndex(keyColumnName);                               // output column name for key (name)
        int valueColIndex = getOutputColumnIndex(valueColumnName);                           // which output column for "value"
        int indexColIndex = getOutputColumnIndex(indexColumnName);                           // output column name for index number

        int rowtimeIndex = inputRows.findColumn("ROWTIME");

        for (int c = 0; c < unpivotCount; c++) {
            unpivotIndex[c] = inputRows.findColumn(unpivotColumnNames[c]);
            // TODO check for missing columns (extra or mismatched names)
            if (unpivotIndex[c] <= 0) {
                throw new SQLException(("No input column found matching "+unpivotColumnNames[c]));
            }
        }

        while (!out.isClosed()) {
            RowEvent e = in.nextRowOrRowtime(1000);

            switch (e) {
            case EndOfStream:
                out.close();
                return;  // end of input

            case Timeout:
                continue;  // timeout ??

            case NewRow:
                long rtime = in.getTimestampInMillis(rowtimeIndex);

                for (int ucol = 0; ucol < unpivotCount; ucol++) {
                    // TODO: is this included in transferColumns
                    //out.setTimestampInMillis(1,rtime);

                    // copy all the passthrough columns
                    // TODO: given we are transferring potentially many columns multiple times, is there setup we could do just once?
                    transferColumns(inputRows, null, results, passMap);

                    // set the ordinal position
                    out.setInt(indexColIndex, ucol+1);
                    // set the key column
                    out.setString(keyColIndex, unpivotColumnNames[ucol]);
                    // set the value column
                    out.setObject(valueColIndex, in.getObject(unpivotIndex[ucol]));
                    out.executeUpdate();
                }
                break;

            case NewRowtimeBound:
                out.setRowtimeBound(in.getRowtimeBound());
                continue;
            }
        }
    }
}
