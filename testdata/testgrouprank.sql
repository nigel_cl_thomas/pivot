
create or replace schema test;
set schema 'TEST';
set path 'TEST';
create stream AccessStream
(
 pageId varchar(1000),
 sourceURL varchar(1000)
);

CREATE FUNCTION group_rank(c cursor,
 rankByColumnName VARCHAR(128),
 rankOutColumnName VARCHAR(128),
 sortOrder VARCHAR(10),
 outputOrder VARCHAR(10),
 maxIdle INTEGER,
 outputMax INTEGER)
 returns table(c.*, groupRank INTEGER)
 language java

 parameter style system defined java

 no sql

 external name 'class com.sqlstream.plugin.grouprank.GroupRank.group_rank';

CREATE VIEW pageCounts1Min AS
SELECT STREAM
pageId, COUNT(*) AS hitCount
FROM AccessStream AS S
GROUP BY
FLOOR(S.ROWTIME TO MINUTE), pageId;

SELECT STREAM pageId, hitCount, groupRank
FROM STREAM
--STREAM keyword is necessary because UDX
--returns an infinite (streaming) result
(
 group_rank
 (
   CURSOR( SELECT STREAM pageId, hitCount FROM pageCounts1Min ),
   'HITCOUNT', 'GROUPRANK', 'desc', 'asc', 10, 5
 )
);


