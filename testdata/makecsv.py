import sys
import csv

writer = csv.writer(sys.stdout)

rows = int(sys.argv[1])
cols = int(sys.argv[2])

headers = []
rowdata = []

headers.append("recnum")
for h in range(0,cols):
    headers.append("h" + str(h+1))

writer.writerow(headers)

for row in range(rows):
    offset=row % 100
    rowdata = [row+1]
    for c in range(0,cols):
        rowdata.append(offset+c+1)
        
    writer.writerow(rowdata)


