-- test the unpivotByName method
--
--

create or replace schema "testpivot";
alter pump "testpivot".* stop;

set schema '"testpivot"';
set path '"testpivot"';

create or replace jar pivot
    LIBRARY 'file:/home/vagrant/nigel/Dropbox/bitbucket/pivot/Pivot.jar'
    OPTIONS(0);

create or replace stream "teststreamin"
( "uid1" BIGINT
, "uid2" VARCHAR(2)
, "h1" INT
, "h2" INT
, "h3" INT
, "h4" INT
, "h4_cf" INT
, "h5" INT
, "h5_cf" INT
, "h6" INT
, "h7" INT
, "h8" INT
, "h9" INT
, "h10" INT
);


create or replace foreign stream "testfilein"
( "uid1" BIGINT
, "uid2" VARCHAR(2)
, "h1" INT
, "h2" INT
, "h3" INT
, "h4" INT
, "h4_cf" INT
, "h5" INT
, "h5_cf" INT
, "h6" INT
, "h7" INT
, "h8" INT
, "h9" INT
, "h10" INT
)
SERVER FILE_SERVER
OPTIONS
( "PARSER" 'CSV'
, "DIRECTORY" '/home/vagrant/nigel/Dropbox/bitbucket/pivot/testdata'
, "FILENAME_PATTERN" 'test.*\.csv'
, "SKIP_HEADER" 'true'
);


create or replace pump "fileinpump" STOPPED
as
insert into "teststreamin"
( "uid1"
, "uid2" 
, "h1"
, "h2"
, "h3"
, "h4"
, "h4_cf"
, "h5"
, "h5_cf"
, "h6"
, "h7"
, "h8"
, "h9"
, "h10"
)
select stream 
  "uid1" 
, "uid2"
, "h1"
, "h2"
, "h3"
, "h4"
, "h4_cf"
, "h5"
, "h5_cf"
, "h6"
, "h7"
, "h8"
, "h9"
, "h10"
from "testfilein"
;


-- add some 'descriptor' columns to 

create or replace view "wideview"
as select stream *
, 'wide column 1 for uid1 '||cast("uid1" as varchar(5)) as "w1"
, 'wide column 2 for uid1 '||cast("uid1" as varchar(5)) as "w2"
, 'wide column 3 for uid1 '||cast("uid1" as varchar(5)) as "w3"
, 'wide column 4 for uid1 '||cast("uid1" as varchar(5)) as "w4"
, 'wide column 5 for uid1 '||cast("uid1" as varchar(5)) as "w5"
from "teststreamin"
;

create or replace stream "testunpivotstream"
( "uid1" BIGINT
, "uid2" VARCHAR(2)
, "w1" varchar(30)
, "w2" varchar(30)
, "w3" varchar(30)
, "w4" varchar(30)
, "w5" varchar(30)
, "key" VARCHAR(128)
, "value" INT
);


CREATE OR REPLACE FUNCTION "unpivotByName"
( "inputRows" CURSOR 
, "unpivotColumnNames" VARCHAR(4096)
, "keyColumnName" VARCHAR(128)
, "valueColumnName" VARCHAR(128)
) RETURNS TABLE
( ROWTIME TIMESTAMP
, "uid1" BIGINT
, "uid2" VARCHAR(2)
, "w1" varchar(30)
, "w2" varchar(30)
, "w3" varchar(30)
, "w4" varchar(30)
, "w5" varchar(30)
, "key" VARCHAR(30)
, "value" INT
)
LANGUAGE JAVA
PARAMETER STYLE SYSTEM DEFINED JAVA
NO SQL
EXTERNAL NAME 'PIVOT:com.sqlstream.udx.UnpivotUdx.unpivotByName';


CREATE OR REPLACE FUNCTION "pivot"
( "inputRows" CURSOR 
, "uidColumnNames" VARCHAR(4096)
, "keyColumnName" VARCHAR(128)
, "valueColumnName" VARCHAR(128)
) RETURNS TABLE
( ROWTIME TIMESTAMP
, "uid1" BIGINT
, "uid2" VARCHAR(2)
, "w1" varchar(30)
, "w2" varchar(30)
, "w3" varchar(30)
, "w4" varchar(30)
, "w5" varchar(30)
, "h1" INT
, "h2" INT
, "h3" INT
, "h4" INT
, "h5" INT
, "h6" INT
, "h7" INT
, "h8" INT
, "h9" INT
, "h10" INT
)
LANGUAGE JAVA
PARAMETER STYLE SYSTEM DEFINED JAVA
NO SQL
EXTERNAL NAME 'PIVOT:com.sqlstream.udx.PivotUdx.execute';

create or replace pump "unpivotpump" STOPPED
as
insert into "testunpivotstream"
( "uid1" 
, "uid2"
, "w1" 
, "w2" 
, "w3" 
, "w4" 
, "w5" 
, "key" 
, "value"
)
select stream * 
from stream("unpivotByName"
  ( CURSOR(select stream * from "wideview")
  , 'h1,h2,h3,h4,h5,h6,h7,h8,h9,h10'
  , 'key'
  , 'value'
  ));

create or replace view "testpivotview"
as
select stream *
from stream ("pivot"
  ( CURSOR (select stream * from "testunpivotstream")
  , 'uid1,uid2'
  , 'key'
  , 'value'
  ));


